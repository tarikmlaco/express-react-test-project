import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import noop from 'lodash/noop';
import styled from 'styled-components';

import { Wrapper, Container } from './Wrappers';

const LoginButton = styled.button`
  padding-top: 5px;
  padding-bottom: 5px;
`;

const TextInput = styled.input`
  margin-top: 5px;
  margin-bottom: 5px;
`;

const Heading = styled.h1``;

const Footer = styled.div`
  flex-direction: row;
`;

const ErrorText = styled.p`
  color: red;
`;

const FooterText = styled.p``;

function handleChange(setterFn) {
  return event => setterFn(event.currentTarget.value);
}

export default function LoginSignup({ submit, isAuthenticated, history, signup, error }) {
  const [oldPassword, setOldPass] = useState('');
  const [newPassword, setNewPass] = useState('');
  return (
    <Wrapper>
      <Container>
        <Heading>{signup ? 'Sign Up' : 'Login'}</Heading>
        <TextInput
          label="Old Password"
          value={oldPassword}
          onChange={handleChange(setOldPass)}
          placeholder="Old Password..."
        />
        <TextInput
          label="New Password"
          value={newPassword}
          onChange={handleChange(setNewPass)}
          placeholder="Old Password..."
        />
        <LoginButton onClick={() => submit({ oldPassword, newPassword })}>
          {signup ? 'Sign Up' : 'Login'}
        </LoginButton>
        {error ? <ErrorText>{error}</ErrorText> : null}
        <Footer>
          <FooterText>{signup ? 'Already signed up?' : "Don't have an account?"}</FooterText>
          {signup ? <Link to="/login">Log In</Link> : <Link to="signup">Sign Up</Link>}
        </Footer>
      </Container>
    </Wrapper>
  );
}

LoginSignup.propTypes = {
  submit: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  history: PropTypes.shape({}).isRequired,
  signup: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
};
