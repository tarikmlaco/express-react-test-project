import React from 'react';
import styled from 'styled-components';

const UserEmail = styled.p`
  padding-left: 10px;
  flex-basis: 50%;
`;

const UserWrap = styled.div`
  display: flex;
  border: solid 1px grey;
  flex-direction: row;
  justify-content: space-between;
`;

const LikeUnlike = styled.button`
  width: 100px;
  background-color: ${props => (props.liked ? 'pink' : 'lightgreen')};
`;

const UserLikes = styled.p``;

export default function User({ me, user, like, unlike }) {
  const liked = user.likes.includes(me.id);
  return (
    <UserWrap>
      <UserEmail>{user.email}</UserEmail>
      <UserLikes>{user.likes.length}</UserLikes>
      <LikeUnlike liked={liked} onClick={liked ? unlike : like}>
        {liked ? 'Unlike' : 'Like'}
      </LikeUnlike>
    </UserWrap>
  );
}
