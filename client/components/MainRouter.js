import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import App from '../containers/App';
import Login from '../containers/Login';
import SignUp from '../containers/SignUp';

import PrivateRoute from '../containers/PrivateRoute';

export default function MainRouter({ checkToken }) {
  useEffect(checkToken, []);
  return (
    <Router>
      <Route path="/">
        <Switch>
          <PrivateRoute
            path="/"
            exact
            authRedirect="/login"
            component={App}
          />
          <Route path="/login" component={Login} />
          <Route path="/signup" component={SignUp} />
        </Switch>
      </Route>
    </Router>
  );
}
