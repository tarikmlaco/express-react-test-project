import React, { useEffect } from 'react';
import styled from 'styled-components';

import { Wrapper, Container } from './Wrappers';
import User from './User';

const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const HeaderText = styled.h1``;

const TableHeading = styled.div`
  display: flex;
  border: solid 1px grey;
  flex-direction: row;
  justify-content: space-between;
  background-color: rgb(240, 240, 240);
`;

const HeadingEmail = styled.p`
  padding-left: 10px;
  flex-basis: 50%;
`;

const LogoutButton = styled.button`
  padding-top: 5px;
  padding-bottom: 5px;
`

const HeadingLikes = styled.p``;

const LikeTitle = styled.p`
  width: 100px;
`;

function handleLikeUser(likeUser, userId) {
  return () => likeUser({ userId });
}

function handleUnlikeUser(unlikeUser, userId) {
  return () => unlikeUser({ userId });
}

function App({ users, mostLiked, me, likeUser, unlikeUser, logOut }) {
  useEffect(() => {
    mostLiked();
  }, [mostLiked]);
  return (
    <Wrapper>
      <Container>
        <Header>
          <HeaderText>Users</HeaderText>
          <LogoutButton onClick={logOut}>LOG OUT</LogoutButton>
        </Header>
        <TableHeading>
          <HeadingEmail>Email</HeadingEmail>
          <HeadingLikes>Likes</HeadingLikes>
          <LikeTitle>Like / Unlike</LikeTitle>
        </TableHeading>
        {users.map(user => (
          <User
            key={user._id}
            me={me}
            user={user}
            like={handleLikeUser(likeUser, user._id)}
            unlike={handleUnlikeUser(unlikeUser, user._id)}
          />
        ))}
      </Container>
    </Wrapper>
  );
}

export default App;
