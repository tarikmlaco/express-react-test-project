import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

export default function PrivateRoute({
  isAuthenticated,
  authRedirect,
  component: Component,
  ...otherProps
}) {
  return (
    <Route
      {...otherProps}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: authRedirect, state: { from: props.location } }} />
        )
      }
    />
  );
}

PrivateRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  authRedirect: PropTypes.string.isRequired,
};
