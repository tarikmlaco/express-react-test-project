import React from 'react';

import { render, cleanup } from 'react-testing-library';
import 'jest-dom/extend-expect';

import LoginSignup from '../LoginSignup';

jest.mock('react-router-dom', () => ({
  Link: 'div',
}));

afterEach(cleanup);

describe('Login/Signup component', () => {
  describe('provided proper props', () => {
    it('renders the component', () => {
      const container = render(
        <LoginSignup
          signup={true}
          isAuthenticated={false}
          history={{}}
          submit={() => null}
          error={''}
        />,
      );
      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
