import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from '../../modules';
import { render, fireEvent, cleanup } from 'react-testing-library'
import noop from 'lodash/noop';

import MainRouter from '../MainRouter';

afterEach(cleanup)

function renderWithRedux(
  ui,
  { initialState, store = createStore(reducer, initialState) } = {}
) {
  return {
    ...render(<Provider store={store}>{ui}</Provider>),
    store,
  }
}


describe('app rendering with router', () => {
  const { container, ...rest } =  renderWithRedux(<MainRouter checkToken={noop} />)
  it('should render the app with router', () => {
    expect(container.innerHTML).toMatchSnapshot();
  });
});

