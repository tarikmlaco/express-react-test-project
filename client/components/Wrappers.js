import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex-basis: 500px;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;
  margin-left: 20px;
  margin-right: 20px;
`;
