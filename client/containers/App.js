import { connect } from 'react-redux';
import App from '../components/App';

import { likeUser, unlikeUser, mostLiked, logOut } from '../modules/actions';

function mapStateToProps(state) {
  return {
    me: state.me,
    users: state.users || [],
  }
}

function mapDispatchToProps(dispatch) {
  return {
    likeUser: ({ userId }) => dispatch(likeUser({ userId })),
    unlikeUser: ({ userId }) => dispatch(unlikeUser({ userId })),
    mostLiked: () => dispatch(mostLiked()),
    logOut: () => dispatch(logOut()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
