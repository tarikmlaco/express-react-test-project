import { connect } from 'react-redux';
import MainRouter from '../components/MainRouter';

import { checkToken } from '../modules/actions';

function mapStateToProps(state) {
  const { isAuthenticated, loading, accessToken } = state;
  return {
    isAuthenticated,
    accessToken,
    loading,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    checkToken: () => dispatch(checkToken()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainRouter);
