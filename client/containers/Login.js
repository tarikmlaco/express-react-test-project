import { connect } from 'react-redux';
import LoginSignup from '../components/LoginSignup';
import { withRouter } from 'react-router-dom';

import { loginAndSaveToken } from '../modules/actions';

function mapStateToProps(state) {
  const { isAuthenticated, error } = state;
  return {
    signup: false,
    isAuthenticated,
    error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    submit: ({ email, password }) => dispatch(loginAndSaveToken({ email, password })),
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(LoginSignup),
);
