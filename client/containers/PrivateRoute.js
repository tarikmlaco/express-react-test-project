import { connect } from 'react-redux';
import PrivateRoute from '../components/PrivateRoute';

function mapStateToProps(state) {
  const { isAuthenticated } = state;
  return {
    isAuthenticated,
  }
}

function mapDispatchToProps() {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
