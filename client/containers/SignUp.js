import { connect } from 'react-redux';
import LoginSignup from '../components/LoginSignup';
import { withRouter } from 'react-router-dom';

import { signUpAndSaveToken } from '../modules/actions';

function mapStateToProps(state) {
  const { isAuthenticated, error } = state;
  return {
    signup: true,
    isAuthenticated,
    error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    submit: ({ email, password }) => dispatch(signUpAndSaveToken({ email, password })),
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(LoginSignup),
);
