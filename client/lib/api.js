import axios from 'axios';
import { apiCalls } from '../modules/constants';

const baseUrl = 'http://localhost:3000/api';
const appendBase = string => `${baseUrl}/${string}`;

const api = {
  login: {
    verb: 'post',
    url: () => appendBase('login'),
    options: ({ email, password }) => ({ email, password }),
    parseResponse: response => ({
      accessToken: response.data.accessToken,
      me: response.data.me,
    }),
    parseError: response => ({ error: response.error }),
  },
  signup: {
    verb: 'post',
    url: () => appendBase('signup'),
    options: ({ email, password }) => ({ email, password }),
    parseResponse: response => ({
      accessToken: response.data.accessToken,
      me: response.data.me,
    }),
    parseError: response => ({ error: response.error }),
  },
  likeUser: {
    verb: 'put',
    url: ({ userId }) => appendBase(`user/${userId}/like`),
    options: () => ({}),
    parseResponse: response => ({ users: response.data }),
    parseError: response => ({ error: response.error }),
  },
  unlikeUser: {
    verb: 'put',
    url: ({ userId }) => appendBase(`user/${userId}/unlike`),
    options: () => ({}),
    parseResponse: response => ({ users: response.data }),
    parseError: response => ({ error: response.error }),
  },
  mostLiked: {
    verb: 'get',
    url: () => appendBase('most-liked'),
    options: () => ({}),
    parseResponse: response => ({ users: response.data }),
    parseError: response => ({ error: response.error }),
  },
  me: {
    verb: 'get',
    url: () => appendBase('me'),
    options: () => ({}),
    parseResponse: response => ({ me: response.data, isAuthenticated: true }),
    parseError: response => ({ isAuthenticated: false, accessToken: null }),
  },
};

export default function apiCall(functionName) {
  return params => {
    return (dispatch, getState) => {
      const { accessToken } = getState();
      dispatch({ type: apiCalls[functionName].request });
      return axios[api[functionName].verb](
        `${api[functionName].url(params)}${accessToken ? `?token=${accessToken}` : ''}`,
        api[functionName].options(params),
      )
        .then(response => {
          dispatch({
            type: apiCalls[functionName].success,
            payload: api[functionName].parseResponse(response),
          });
        })
        .catch(err => {
          console.log('api error: ', err);
          dispatch({
            type: apiCalls[functionName].error,
            payload: api[functionName].parseError(err),
          });
        });
    };
  };
}
