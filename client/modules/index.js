import { apiCalls, simpleActions } from './constants';

const initialState = {
  users: [],
  me: {},
  loading: false,
  error: '',
  accessToken: null,
  isAuthenticated: false,
};

export default function rootReducer(state = initialState, action) {
  switch (action.type) {
    case apiCalls.login.request:
    case apiCalls.signup.request:
    case apiCalls.me.request:
    case apiCalls.updatePassword.request:
    case apiCalls.userById.request:
    case apiCalls.likeUser.request:
    case apiCalls.unlikeUser.request:
    case apiCalls.mostLiked.request:
      return { ...state, loading: true };

    case apiCalls.login.error:
    case apiCalls.signup.error:
    case apiCalls.updatePassword.error:
    case apiCalls.userById.error:
    case apiCalls.likeUser.error:
    case apiCalls.unlikeUser.error:
    case apiCalls.mostLiked.error:
      return { ...state, loading: false, error: JSON.stringify(action.error) };

    case apiCalls.me.error:
      return {
        ...state,
        loading: false,
        error: JSON.stringify(action.error),
        isAuthenticated: false,
        accessToken: null,
      };

    case apiCalls.login.success:
    case apiCalls.signup.success:
      return {
        ...state,
        loading: false,
        me: action.payload.me,
        accessToken: action.payload.accessToken,
        isAuthenticated: true,
      };

    case apiCalls.likeUser.success:
    case apiCalls.unlikeUser.success:
    case apiCalls.mostLiked.success:
      return {
        ...state,
        loading: false,
        users: action.payload.users,
      };

    case apiCalls.me.success:
      return {
        ...state,
        me: action.payload.me,
        loading: false,
        isAuthenticated: true,
      };

    case simpleActions.setToken:
      return {
        ...state,
        loading: false,
        accessToken: action.payload.accessToken,
      };

    case simpleActions.removeToken:
      return {
        ...state,
        accessToken: null,
        isAuthenticated: false,
      };

    default:
      return state;
  }
}
