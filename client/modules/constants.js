import { matrix } from 'deep-key-mirror';

export const apiCallNames = [
  'login',
  'signup',
  'me',
  'updatePassword',
  'userById',
  'likeUser',
  'unlikeUser',
  'mostLiked',
  'me',
]

export const apiCallStates = [
  'request',
  'success',
  'error',
];

export const apiCalls = matrix([apiCallNames, apiCallStates]);

export const simpleActions = {
  setToken: 'setToken',
  removeToken: 'removeToken',
};
