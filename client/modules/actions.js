import apiCall from '../lib/api';
import { simpleActions } from './constants';

export const me = apiCall('me');
export const login = apiCall('login');
export const signup = apiCall('signup');
export const likeUser = apiCall('likeUser');
export const unlikeUser = apiCall('unlikeUser');
export const mostLiked = apiCall('mostLiked');

export function signUpAndSaveToken({ email, password }) {
  return (dispatch, getState) => {
    dispatch(signup({ email, password })).then(() => {
      const { accessToken } = getState();
      localStorage.setItem('accessToken', accessToken);
    });
  };
}

export function loginAndSaveToken({ email, password }) {
  return (dispatch, getState) => {
    dispatch(login({ email, password })).then(() => {
      const { accessToken } = getState();
      localStorage.setItem('accessToken', accessToken);
    });
  };
}

export function checkToken() {
  return (dispatch, getState) => {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      dispatch({ type: simpleActions.setToken, payload: { accessToken } });
      dispatch(me());
    }
  };
}

export function logOut() {
  return (dispatch) => {
    localStorage.removeItem('accessToken');
    dispatch({ type: simpleActions.removeToken });
  }
}
