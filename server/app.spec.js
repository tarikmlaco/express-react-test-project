import 'babel-polyfill';
const request = require('supertest');
const uuid = require('uuidv4');
const app = require('./app');

const LOGIN_PATH = '/api/login';
const SIGNUP_PATH = '/api/signup';
const ME_PATH = token => `/api/me?token=${token}`;
const LIKE_PATH = (id, token) => `/api/user/${id}/like?token=${token}`;
const UNLIKE_PATH = (id, token) => `/api/user/${id}/unlike?token=${token}`;
const GET_USER_PATH = id => `/api/user/${id}`;
const MOST_LIKED_PATH = `/api/most-liked`;

const uniqueEmail = () => `${uuid()}@test.com`;
const uniquePassword = () => uuid();

const login = ({ email, password }) =>
  request(app)
    .post(LOGIN_PATH)
    .set('Accept', 'application/json')
    .send({ email, password });

const signup = ({ email, password }) =>
  request(app)
    .post(SIGNUP_PATH)
    .set('Accept', 'application/json')
    .send({ email, password });

const me = token =>
  request(app)
    .get(ME_PATH(token))
    .set('Accept', 'application/json')
    .send();

const mostLiked = () =>
  request(app)
    .get(MOST_LIKED_PATH)
    .set('Accept', 'application/json')
    .send();

const getUser = id =>
  request(app)
    .get(GET_USER_PATH(id))
    .set('Accept', 'application/json')
    .send();

const like = ({ userToken, userId }) =>
  request(app)
    .put(LIKE_PATH(userId, userToken))
    .set('Accept', 'application/json')
    .send();

const unlike = ({ userToken, userId }) =>
  request(app)
    .put(UNLIKE_PATH(userId, userToken))
    .set('Accept', 'application/json')
    .send();

describe('User liking app', () => {
  const email = uniqueEmail();
  const password = uniquePassword();

  const seededEmail = 'tarikmlaco@gmail.com';
  const seededId = '5cd87ec84f4cd48e5206b85d';

  let token = undefined;

  describe('When calling signup', () => {
    describe('When signup details are unique and complete', () => {
      it('Responds with a new user details', async () => {
        return signup({ email, password })
          .expect(200)
          .then(response => {
            expect(response.body.me.email).toEqual(email);
            expect(response.body.me.likes).toEqual([]);
          });
      });
    });

    describe('When signup details are repeated', () => {
      it('Responds with an error', async () => {
        return signup({ email, password })
          .expect(500)
          .then(response => {
            expect(response.body.error).toBe('Error registering new user, please try again.');
          });
      });
    });
  });

  describe('When calling login', () => {
    const fakeEmail = uniqueEmail();
    const fakePassword = uniquePassword();
    describe('When login details are correct', () => {
      it('Responds with an OK status and sets a token', async () => {
        return login({ email, password })
          .expect(200)
          .then(response => {
            token = response.body.accessToken;
            expect(response.body.accessToken).toBeDefined();
          });
      });
    });

    describe('When login details are incorrect', () => {
      it('Responds with an error', () => {
        return login({ email: fakeEmail, password: fakePassword })
          .expect(401)
          .then(response => {
            expect(response.body.error).toBe('Incorrect email or password');
          });
      });
    });
  });

  describe('When calling me', () => {
    describe('When provided token is correct', () => {
      it('Returns user details', () => {
        return me(token)
          .expect(200)
          .then(response => {
            expect(response.body.email).toEqual(email);
            expect(response.body.likes).toEqual([]);
          });
      });
    });
  });

  describe('When calling user/:id/like', () => {
    describe('When provided token and user id are correct', () => {
      it('Returns a list of users', () => {
        return like({ userToken: token, userId: seededId })
          .expect(200)
          .then(response => {
            expect(response.body).toEqual(
              expect.arrayContaining([
                expect.objectContaining({
                  email: expect.any(String),
                  _id: expect.any(String),
                  likes: expect.any(Array),
                }),
              ]),
            );
          });
      });
    });
  });

  describe('When calling user/:id/unlike', () => {
    describe('When provided token and user id are correct', () => {
      it('Returns a list of users', () => {
        return unlike({ userToken: token, userId: seededId })
          .expect(200)
          .then(response => {
            expect(response.body).toEqual(
              expect.arrayContaining([
                expect.objectContaining({
                  email: expect.any(String),
                  _id: expect.any(String),
                  likes: expect.any(Array),
                }),
              ]),
            );
          });
      });
    });
  });

  describe('When calling user/:id', () => {
    describe('When provided user id', () => {
      it('Returns user details', () => {
        return getUser(seededId)
          .expect(200)
          .then(response => {
            expect(response.body.email).toEqual(seededEmail);
            expect(response.body.likes).toEqual(expect.any(Array));
          });
      });
    });
  });

  describe('When calling /most-liked', () => {
    it('Returns a list of users', () => {
      return mostLiked()
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            expect.arrayContaining([
              expect.objectContaining({
                email: expect.any(String),
                _id: expect.any(String),
                likes: expect.any(Array),
              }),
            ]),
          );
        });
    });
  });
});
