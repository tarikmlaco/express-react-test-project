import jwt from 'jsonwebtoken';

const secret = process.env.MY_SECRET;

const withAuth = function(req, res, next) {
  const token =
      req.body.token ||
      req.query.token ||
      req.headers['x-access-token'] ||
      req.cookies.token;

  if (!token) {
    res.status(401).json({ error: 'Unauthorized: No token provided' });
  } else {
    jwt.verify(token, secret, function(err, decoded) {
      if (err) {
        res.status(401).json({ error: 'Unauthorized: Invalid token' });
      } else {
        req.user = decoded;
        next();
      }
    });
  }
}

export default withAuth;
