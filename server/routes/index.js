import express from 'express';
import User from '../models/User';

import withAuth from '../middleware';
import jwt from 'jsonwebtoken';

const router = express.Router();
const secret = process.env.MY_SECRET;

function sortedUsers() {
  return User.aggregate([
    { $project: { _id: 1, email: 1, likes: 1, likes_count: { $size: '$likes' } } },
    {
      $sort: { likes_count: -1 },
    },
  ]);
}

router.post('/signup', async (req, res) => {
  const { email, password } = req.body;
  const user = new User({ email, password });
  try {
    const newUser = await user.save();
    const payload = { email, id: newUser._id };
    const token = jwt.sign(payload, secret, { expiresIn: '1h' });
    res.status(200).json({
      me: { email: newUser.email, id: newUser._id, likes: newUser.likes },
      accessToken: token,
    });
  } catch (err) {
    res.status(500).json({ error: 'Error registering new user, please try again.' });
  }
});

router.post('/login', async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await User.findOne({ email });
    if (user) {
      const correctPassword = await user.isCorrectPassword(password);
      if (correctPassword) {
        const payload = { email, id: user.id };
        const token = jwt.sign(payload, secret, { expiresIn: '1h' });
        res
          .status(200)
          .json({
            accessToken: token,
            me: { email, likes: user.likes, id: user.id },
          });
      } else {
        res.status(401).json({ error: 'Incorrect email or password' });
      }
    } else {
      res.status(401).json({ error: 'Incorrect email or password' });
    }
  } catch (err) {
    res.status(500).json({ error: JSON.stringify(err) });
  }
});

router.get('/me', withAuth, async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.user.id });
    res.json({ id: user._id, email: user.email, likes: user.likes });
  } catch (err) {
    res.json({ error: err });
  }
});

router.put('/me/update-password', withAuth, async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  try {
    const user = await User.findOne({ _id: req.user.id });
    const passwordCorrect = await user.isCorrectPassword(oldPassword);
    if (passwordCorrect && newPassword) {
      user.password = newPassword;
      await user.save();
      res.json({ email: user.email, likes: user.likes, id: user._id });
    } else {
      res.status(400).json({ error: 'Old password is not correct or new password is missing!' });
    }
  } catch (err) {
    res.status(500).json({ error: JSON.stringify(err) });
  }
});

router.get('/user/:id', async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    res.json({ email: user.email, likes: user.likes });
  } catch (err) {
    res.json({ error: 'Could not find a user with this ID' });
  }
});

router.put('/user/:id/like', withAuth, async (req, res) => {
  try {
    await User.findOneAndUpdate({ _id: req.params.id }, { $addToSet: { likes: [req.user.id] } });
    const users = await sortedUsers();
    res.json(users);
  } catch (err) {
    res.send(400).json(err);
  }
});

router.put('/user/:id/unlike', withAuth, async (req, res) => {
  try {
    await User.findOneAndUpdate({ _id: req.params.id }, { $pull: { likes: req.user.id } });
    const users = await sortedUsers();
    res.json(users);
  } catch (err) {
    res.send(400).json(err);
  }
});

router.get('/most-liked', async (req, res) => {
  const users = await sortedUsers();
  res.json(users);
});

router.get('/', (req, res) => {
  res.render('index', { title: 'Express' });
});

export default router;
