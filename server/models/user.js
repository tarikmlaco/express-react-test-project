import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const ObjectId = mongoose.Schema.Types.ObjectId;
const saltRounds = 10;

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  likes: [{ type: ObjectId, ref: 'User' }],
});

UserSchema.pre('save', async function() {
  if (this.isNew || this.isModified('password')) {
    const document = this;
    try {
      const hashedPassword = await bcrypt.hash(this.password, saltRounds);
      document.password = hashedPassword;
    } catch (err) {
      throw new Error(err);
    }
  }
});

UserSchema.methods.isCorrectPassword = async function(password) {
  try {
    const isSame = await bcrypt.compare(password, this.password);
    return isSame;
  } catch (err) {
    throw new Error(err);
  }
}

export default mongoose.model('User', UserSchema);
